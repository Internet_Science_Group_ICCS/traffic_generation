SYN_TCP ATTACK

This was created for SerIoT project H2020 to illustrate how SYN attacks are launched. 


Tested and developed with Python2.4.

Usage
You may need to run the program as root.

How to execute:
  syntcp_attack.py <dst_ip> <dst_port> [--sleep=<sec>] [--verbose] [--very-verbose]

Options:
  -h, --help            Show this screen.
  --version             Show version.
  --sleep=<seconds>     How many seconds to sleep betseen scans [default: 0].
  --verbose             Show addresses being spoofed. [default: False]
  --very-verbose        Display everything. [default: False]
  
  
-----------------------------------------------------------------------------------------------------------------------
  
  
BENIGN GENERATOR

This was created for SerIoT project H2020 to immitate potential clients that establish full TCP connections with a server. 


Tested and developed with Python3.6.

Usage
You may need to run the program as root.

How to execute:
  b9generator.py <server's IP> <server's port> <number of connections>
  
  

-----------------------------------------------------------------------------------------------------------------------
  
  
  
VIRTUAL SERVER

This was created for SerIoT project H2020 to immitate a potential server. 


Tested and developed with Python3.6.

Usage
You may need to run the program as root.

How to execute:
  server.py <server's IP> <server's port> 

